#!/usr/bin/env node
const fs = require("fs");
const program = require("commander");

program.version("1.0.0").description("TASK FOR BACKEND DEVELOPER");

program
  .argument("<file_log>")
  .option("-t, --konversi <konversi_file>", "Convert to json or txt file")
  .option("-o, --output <output_file>", "choose where to put the output file")
  .action((file_log, options, command) => {
    let cutExtension = file_log.split(".");
    cutExtension = cutExtension[0].split("/").pop();
    if (options.konversi == undefined && options.output == undefined) {
      fs.copyFile(`${file_log}`, `${__dirname}/${cutExtension}.txt`, (err) => {
        if (!err) {
          console.log("success");
        } else {
          console.log(err.message);
        }
      });
    } else {
      if (options.konversi == "json") {
        if (options.output == undefined) {
          fs.copyFile(
            `${file_log}`,
            `${__dirname}/${cutExtension}.json`,
            (err) => {
              if (!err) {
                console.log("success");
              } else {
                console.log(err.message);
              }
            }
          );
        } else {
          fs.copyFile(`${file_log}`, `${options.output}`, (err) => {
            if (!err) {
              console.log("success");
            } else {
              console.log(err.message);
            }
          });
        }
      } else if (options.konversi == "txt") {
        if (options.output == undefined) {
          fs.copyFile(
            `${file_log}`,
            `${__dirname}/${cutExtension}.txt`,
            (err) => {
              if (!err) {
                console.log("success");
              } else {
                console.log(err.message);
              }
            }
          );
        } else {
          fs.copyFile(`${file_log}`, `${options.output}`, (err) => {
            if (!err) {
              console.log("success");
            } else {
              console.log(err.message);
            }
          });
        }
      } else if (options.output) {
        fs.copyFile(`${file_log}`, `${options.output}`, (err) => {
          if (!err) {
            console.log("success");
          } else {
            console.log(err.message);
          }
        });
      } else {
        console.log("Extension format must be json or txt");
      }
    }
  });

program.addHelpText(
  "after",
  `
How to use:
  - Get default log file
  $ task-telkom /var/log/system.log
  - Convert to json or txt file
  $ task-telkom /var/log/system.log -t json
  $ task-telkom /var/log/system.log -t txt
  - Choose where to put the output file
  $ task-telkom /var/log/system.log -o /Users/admin/Desktop/system.txt
  - or
  $ task-telkom /var/log/system.log -t json -o /Users/admin/Desktop/system.json
  `
);

program.parse(process.argv);
