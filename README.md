# Command Line Tools CLI

TASK FOR BACKEND DEVELOPER (Telkom)

# Getting started

### Installation

Install the dependencies

```sh
$ npm install
```

### Create Symlink

```sh
$ npm link
```

### How to use:

Get default log file

```sh
$ task-telkom /var/log/system.log
```

Convert to json or txt file

```sh
$ task-telkom /var/log/system.log -t json
```

```sh
$ task-telkom /var/log/system.log -t txt
```

Choose where to put the output file

```sh
$ task-telkom /var/log/system.log -o /Users/admin/Desktop/system.txt
```

or

```sh
$ task-telkom /var/log/system.log -t json -o /Users/admin/Desktop/system.json
```
